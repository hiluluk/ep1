#include "../inc/exploder.hpp"

#include <iostream>
#include <stdlib.h>

#define VIVA '+'

Exploder::Exploder(){
  tipo = "Exploder";
  linhaInicial = 7;
  colunaInicial = 36;
  int linha[12]={0,1,2,3,4,0,4,0,1,2,3,4};
  int coluna[12]={0,0,0,0,0,2,2,4,4,4,4,4};
  for (int aux = 0; aux < 12; aux++) {
    celulas[linha[aux] + linhaInicial][coluna[aux] + colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
void Exploder::setLinhaInicial(int linhaInicial){
  if(linhaInicial >= TAML/2-4 or linhaInicial < 0){
    cout << "Digite um valor para linha menor que " << TAML/2-4 << " e não negativo." << '\n';
    linhaInicial = levalidaPosicao(TAML/2-4);
    system("clear");
  }
  this->linhaInicial = linhaInicial;
}
////////////////////////////////////////////////////////////////////////////////
void Exploder::setColunaInicial(int colunaInicial){
  if(colunaInicial >= TAMC/2-4 or colunaInicial < 0){
    cout << "Digite um valor para coluna menor que " << TAMC/2-4 << " e não negativo." << '\n';
    colunaInicial = levalidaPosicao(TAMC/2-4);
    system("clear");
  }
  this->colunaInicial = colunaInicial;
}
////////////////////////////////////////////////////////////////////////////////
Exploder::Exploder(int linhaInicial,int colunaInicial){
  tipo = "Exploder";
  setLinhaInicial(linhaInicial);
  setColunaInicial(colunaInicial);
  int linha[12]={0,1,2,3,4,0,4,0,1,2,3,4};
  int coluna[12]={0,0,0,0,0,2,2,4,4,4,4,4};
  for (int aux = 0; aux < 12; aux++) {
    celulas[linha[aux] + this->linhaInicial][coluna[aux] + this->colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
Exploder::~Exploder(){}
