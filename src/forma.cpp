#include "../inc/forma.hpp"

#include <iostream>
#include <stdlib.h>

Forma::Forma(){
   tipo = "";
   linhaInicial = 0;
   colunaInicial = 0;
}
////////////////////////////////////////////////////////////////////////////////
Forma::Forma(string tipo,int linhaInicial,int colunaInicial) {
   this->tipo = tipo;
   this->linhaInicial = linhaInicial;
   this->colunaInicial = colunaInicial;
}
////////////////////////////////////////////////////////////////////////////////
Forma::~Forma(){}
////////////////////////////////////////////////////////////////////////////////
string Forma::getTipo(){
   return tipo;
}
////////////////////////////////////////////////////////////////////////////////
void Forma::setTipo(string tipo){
   this->tipo = tipo;
}
////////////////////////////////////////////////////////////////////////////////
int Forma::getLinhaInicial(){
   return linhaInicial;
}
////////////////////////////////////////////////////////////////////////////////
int Forma::levalidaPosicao(int maximo){
  string posicao;
  cin >> posicao;
  while (verificaPosicao(posicao, maximo)) {
    cout << "Digite um valor menor que " << maximo << " e não negativo:" << '\n';
    cin >> posicao;
  }
  return atoi(posicao.c_str());
}
////////////////////////////////////////////////////////////////////////////////
int Forma::verificaPosicao(string posicao,int maximo){
  int tamanho = posicao.size();
  for (int aux = 0; aux < tamanho; aux++) {
    if(!isdigit(posicao[aux])){
      return 1;
    }
  }
  if (atoi(posicao.c_str()) < 0 or atoi(posicao.c_str()) >= maximo) {
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////
void Forma::setLinhaInicial(int linhaInicial){
  if(linhaInicial >= TAML/2 or linhaInicial < 0){
    cout << "Digite um valor para linha menor que " << TAML/2 << " e não negativo." << '\n';
    linhaInicial = levalidaPosicao(TAML/2);
    system("clear");
  }
  this->linhaInicial = linhaInicial;
}
////////////////////////////////////////////////////////////////////////////////
int Forma::getColunaInicial(){
   return colunaInicial;
}
////////////////////////////////////////////////////////////////////////////////
void Forma::setColunaInicial(int colunaInicial){
  if(colunaInicial >= TAMC/2 or colunaInicial < 0){
    cout << "Digite um valor para coluna menor que " << TAMC/2 << " e não negativo." << '\n';
    colunaInicial = levalidaPosicao(TAMC/2);
    system("clear");
  }
  this->colunaInicial = colunaInicial;
}
