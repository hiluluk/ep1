#ifndef TENCELLROW_HPP
#define TENCELLROW_HPP

#include "forma.hpp"

class TenCellRow : public Forma{
  public:
    TenCellRow();
    TenCellRow(int linhaInicial,int colunaInicial);
    ~TenCellRow();
    void setColunaInicial(int colunaInicial);
};
#endif
