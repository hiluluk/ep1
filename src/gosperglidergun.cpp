#include "../inc/gosperglidergun.hpp"

#include <iostream>
#include <stdlib.h>

#define VIVA '+'

GosperGliderGun::GosperGliderGun(){
  tipo = "GosperGliderGun";
  linhaInicial = 1;
  colunaInicial = 19;
  int linha[36]={4,5,4,5,4,5,6,3,7,2,8,2,8,5,3,7,4,5,6,5,2,3,4,2,3,4,1,5,0,1,5,6,2,3,2,3};
  int coluna[36]={0,0,1,1,10,10,10,11,11,12,12,13,13,14,15,15,16,16,16,17,20,20,20,21,21,21,22,22,24,24,24,24,34,34,35,35};
  for (int aux = 0; aux < 36; aux++) {
    celulas[linha[aux] + linhaInicial][coluna[aux] + colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
void GosperGliderGun::setLinhaInicial(int linhaInicial){
  if(linhaInicial >= TAML/2 - 8 or linhaInicial < 0){
    cout << "Digite um valor para linha menor que " << TAML/2-8 << " e não negativo." << '\n';
    linhaInicial = levalidaPosicao(TAML/2-8);
    system("clear");
  }
  this->linhaInicial = linhaInicial;
}
////////////////////////////////////////////////////////////////////////////////
void GosperGliderGun::setColunaInicial(int colunaInicial){
  if(colunaInicial >= TAMC/2-35 or colunaInicial < 0){
    cout << "Digite um valor para coluna menor que " << TAMC/2-35 << " e não negativo." << '\n';
    colunaInicial = levalidaPosicao(TAMC/2-35);
    system("clear");
  }
  this->colunaInicial = colunaInicial;
}
////////////////////////////////////////////////////////////////////////////////
GosperGliderGun::GosperGliderGun(int linhaInicial,int colunaInicial){
  tipo = "GosperGliderGun";
  setLinhaInicial(linhaInicial);
  setColunaInicial(colunaInicial);
  int linha[36]={4,5,4,5,4,5,6,3,7,2,8,2,8,5,3,7,4,5,6,5,2,3,4,2,3,4,1,5,0,1,5,6,2,3,2,3};
  int coluna[36]={0,0,1,1,10,10,10,11,11,12,12,13,13,14,15,15,16,16,16,17,20,20,20,21,21,21,22,22,24,24,24,24,34,34,35,35};
  for (int aux = 0; aux < 36; aux++) {
    celulas[linha[aux] + this->linhaInicial][coluna[aux] + this->colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
GosperGliderGun::~GosperGliderGun(){}
