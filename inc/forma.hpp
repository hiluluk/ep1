#ifndef FORMA_HPP
#define FORMA_HPP

#include "campo.hpp"

#include <string>

using namespace std;

class Forma:public Campo {
  protected:
      string tipo;
      int linhaInicial;
      int colunaInicial;

   public:
      Forma();
      Forma(string tipo,int linhaInicial,int colunaInicial);
      ~Forma();
      string getTipo();
      void setTipo(string tipo);
      int getLinhaInicial();
      virtual void setLinhaInicial(int linhaInicial);
      int getColunaInicial();
      virtual void setColunaInicial(int colunaInicial);
      int levalidaPosicao(int maximo);
      int verificaPosicao(string posicao,int maximo);

};
#endif
