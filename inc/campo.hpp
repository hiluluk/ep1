#ifndef CAMPO_HPP
#define CAMPO_HPP

#define TAML 44
#define TAMC 160

using namespace std;
class Campo {
  protected:
    char celulas[TAML][TAMC];

  public:
    Campo();
    Campo(char estado);
    ~Campo();
    char getCelulas(int linha,int coluna);
    void setCelulas(int linha,int coluna,char estado);

    void mostraCampo();
    void passaGeracao();

};
#endif
