#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <cctype>
#include <string>

#include "campo.hpp"
#include "forma.hpp"
#include "exploder.hpp"
#include "gosperglidergun.hpp"
#include "tencellrow.hpp"

int menu(void);
int menuFormas(int *linha,int *coluna);
int verificaGeracoes(string geracoes);
int leValidaGeracoes(void);
int verificaPosicao(string posicao);
int levalidaPosicao(void);
int verificaForma(string forma);

using namespace std;

int main(int argc, char const *argv[]) {
  string continua;
  int geracoes,loops,opcao;
  int linha,coluna;
  Forma *matriz = new Forma();
  do {
    system("clear");
    opcao = menu();
    if(opcao == 0){
      break;
    }
    switch (opcao) {
      case 1:
        {
          if (menuFormas(&linha,&coluna)) {
            Exploder *matrizForma = new Exploder(linha,coluna);
            matriz = matrizForma;
            matrizForma->~Exploder();
          } else {
            Exploder *matrizForma = new Exploder();
            matriz = matrizForma;
            matrizForma->~Exploder();
          }
        }
        break;
      case 2:
        {
          if (menuFormas(&linha,&coluna)) {
            GosperGliderGun *matrizForma = new GosperGliderGun(linha,coluna);
            matriz = matrizForma;
            matrizForma->~GosperGliderGun();
          } else {
            GosperGliderGun *matrizForma = new GosperGliderGun();
            matriz = matrizForma;
            matrizForma->~GosperGliderGun();
          }
        }
        break;
      case 3:
        {
          if (menuFormas(&linha,&coluna)) {
            TenCellRow *matrizForma = new TenCellRow(linha,coluna);
            matriz = matrizForma;
            matrizForma->~TenCellRow();
          } else {
            TenCellRow *matrizForma = new TenCellRow();
            matriz = matrizForma;
            matrizForma->~TenCellRow();
          }
        }
        break;
    };
    system("clear");
    matriz->mostraCampo();
    geracoes = leValidaGeracoes();
    for (loops = 0; loops < geracoes; loops++) {
      system("clear");
      matriz->passaGeracao();
      matriz->mostraCampo();
      std::cout << loops+1 << '\n';
      usleep(100000);
    }
    cout << "Digite S para voltar ao menu ou qualquer outra tecla para sair." << '\n';
    cin >> continua;
    continua[0] = toupper(continua[0]);
    system("clear");
  } while(continua[0] == 'S');
  matriz->~Forma();
  return 0;
}
////////////////////////////////////////////////////////////////////////////////
int menu(void) {
  string forma;
  do {
    cout << "Escolha uma das formas a seguir:" << '\n';
    cout << "0 - Sair do programa." << '\n';
    cout << "1 - Exploder." << '\n';
    cout << "2 - Gosper Glider Gun." << '\n';
    cout << "3 - 10 Cell Row." << '\n';
    cin >> forma;
    system("clear");
  } while(verificaForma(forma));
  return atoi(forma.c_str());
}
////////////////////////////////////////////////////////////////////////////////
int menuFormas(int *linha,int *coluna){
  string opcao;
  cout << "Digite S para escolher a posição inicial da forma ou qualquer outra tecla para usar a padrão." << '\n';
  cin >> opcao;
  system("clear");
  opcao[0] = toupper(opcao[0]);
  if (opcao[0] == 'S') {
    cout << "Digite a linha inicial que deseja:" << '\n';
    *linha = levalidaPosicao();
    system("clear");
    cout << "Digite a coluna inicial que deseja:" << '\n';
    *coluna = levalidaPosicao();
    system("clear");
    return 1;
  } else {
    return 0;
  }
}
////////////////////////////////////////////////////////////////////////////////
int leValidaGeracoes(void){
  string geracoes;
  cout << "Digite o número de gerações que deseja acompanhar:" << '\n';
  cin >> geracoes;
  while (verificaGeracoes(geracoes)) {
    cout << "Digite um valor maior ou igual a 0:" << '\n';
    cin >> geracoes;
  }
  return atoi(geracoes.c_str());
}
////////////////////////////////////////////////////////////////////////////////
int verificaGeracoes(string geracoes){
  int tamanho = geracoes.size();
  for (int aux = 0; aux < tamanho; aux++) {
    if(!isdigit(geracoes[aux])){
      return 1;
    }
  }
  if (atoi(geracoes.c_str()) < 0) {
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////
int levalidaPosicao(void){
  string posicao;
  cin >> posicao;
  while (verificaPosicao(posicao)) {
    cout << "Digite um valor maior ou igual a 0:" << '\n';
    cin >> posicao;
  }
  return atoi(posicao.c_str());
}
////////////////////////////////////////////////////////////////////////////////
int verificaPosicao(string posicao){
  int tamanho = posicao.size();
  for (int aux = 0; aux < tamanho; aux++) {
    if(!isdigit(posicao[aux])){
      return 1;
    }
  }
  if (atoi(posicao.c_str()) < 0) {
    return 1;
  }
  return 0;
}
////////////////////////////////////////////////////////////////////////////////
int verificaForma(string forma){
  int tamanho = forma.size();
  for (int aux = 0; aux < tamanho; aux++) {
    if(!isdigit(forma[aux])){
      cout << "Digite um valor válido!" << '\n' << '\n';
      return 1;
    }
  }
  if (atoi(forma.c_str()) < 0 or atoi(forma.c_str()) > 3) {
    cout << "Digite um valor válido!" << '\n' << '\n';
    return 1;
  }
  return 0;
}
