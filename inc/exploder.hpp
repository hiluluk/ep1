#ifndef EXPLODER_HPP
#define EXPLODER_HPP

#include "forma.hpp"

class Exploder : public Forma{
  public:
    Exploder();
    Exploder(int linhaInicial,int colunaInicial);
    ~Exploder();
    void setLinhaInicial(int linhaInicial);
    void setColunaInicial(int colunaInicial);
};
#endif
