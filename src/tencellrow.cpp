#include "../inc/tencellrow.hpp"

#include <iostream>
#include <stdlib.h>

#define VIVA '+'

TenCellRow::TenCellRow(){
  tipo ="TenCellRow";
  linhaInicial = 11;
  colunaInicial = 34;
  int linha[10]={0,0,0,0,0,0,0,0,0,0};
  int coluna[10]={0,1,2,3,4,5,6,7,8,9};
  for (int aux = 0; aux < 10; aux++) {
    celulas[linha[aux] + linhaInicial][coluna[aux] + colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
void TenCellRow::setColunaInicial(int colunaInicial){
  if(colunaInicial >= TAMC/2-9 or colunaInicial < 0){
    cout << "Digite um valor para coluna menor que " << TAMC/2-9 << " e não negativo." << '\n';
    colunaInicial = levalidaPosicao(TAMC/2-9);
    system("clear");
  }
  this->colunaInicial = colunaInicial;
}
////////////////////////////////////////////////////////////////////////////////
TenCellRow::TenCellRow(int linhaInicial,int colunaInicial){
  tipo ="TenCellRow";
  setLinhaInicial(linhaInicial);
  setColunaInicial(colunaInicial);
  int linha[10]={0,0,0,0,0,0,0,0,0,0};
  int coluna[10]={0,1,2,3,4,5,6,7,8,9};
  for (int aux = 0; aux < 10; aux++) {
    celulas[linha[aux] + this->linhaInicial][coluna[aux] + this->colunaInicial] = VIVA;
  }
}
////////////////////////////////////////////////////////////////////////////////
TenCellRow::~TenCellRow(){}
