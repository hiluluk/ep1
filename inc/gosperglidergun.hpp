#ifndef GOSPERGLIDERGUN_HPP
#define GOSPERGLIDERGUN_HPP

#include "forma.hpp"

class GosperGliderGun : public Forma{
  public:
    GosperGliderGun();
    GosperGliderGun(int linhaInicial,int colunaInicial);
    ~GosperGliderGun();
    void setLinhaInicial(int linhaInicial);
    void setColunaInicial(int colunaInicial);
};
#endif
