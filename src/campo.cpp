#include "../inc/campo.hpp"

#include <iostream>

using namespace std;

#define VIVA '+'
#define MORTA '_'

Campo::Campo(){
  for(int linha = 0; linha < TAML; linha++) {
    for (int coluna = 0; coluna < TAMC; coluna++) {
      celulas[linha][coluna] = MORTA;
    }
  }
}
Campo::Campo(char estado) {
  for (int linha = 0; linha < TAML; linha++) {
    for (int coluna = 0; coluna < TAMC; coluna++) {
      celulas[linha][coluna] = estado;
    }
  }
}
Campo::~Campo(){}

char Campo::getCelulas(int linha,int coluna){
   return celulas[linha][coluna];
}
void Campo::setCelulas(int linha,int coluna,char estado){
   celulas[linha][coluna] = estado;
}
void Campo::mostraCampo() {
  for (int linha = 0; linha < TAML/2; linha++){
    for (int coluna = 0; coluna < TAMC/2; coluna++) {
      cout <<  celulas[linha][coluna];
    }
    cout << '\n';
  }
}

void clonaCampo(char clone[][TAMC],char campo[][TAMC]);
char aplicaRegras(char clone[][TAMC],int linha, int coluna);
int contaVizinhos(char clone[][TAMC],int linha,int coluna,int modo);
////////////////////////////////////////////////////////////////////////////////
void Campo::passaGeracao(){
  char clone[TAML][TAMC];
  int linha,coluna;
  clonaCampo(clone,celulas);
  for (linha = 0; linha < TAML; linha++) {
    for (coluna = 0; coluna < TAMC; coluna++) {
      celulas[linha][coluna] = aplicaRegras(clone,linha,coluna);
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
void clonaCampo(char clone[][TAMC],char campo1[][TAMC]) {
  int linha,coluna;
  for (linha = 0; linha < TAML; linha++) {
    for (coluna = 0; coluna < TAMC; coluna++) {
      clone[linha][coluna] = campo1[linha][coluna];
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
char aplicaRegras(char clone[][TAMC],int linha, int coluna){
  int vizinhosVivos;
  if (!linha or linha == TAML-1){
    if (!coluna or coluna == TAMC-1) {
      vizinhosVivos = contaVizinhos(clone,linha,coluna,1);
    } else {
      vizinhosVivos = contaVizinhos(clone,linha,coluna,2);
    }
  }
  else if (!coluna or coluna == TAMC-1){
    vizinhosVivos = contaVizinhos(clone,linha,coluna,2);
  }
  else{
    vizinhosVivos = contaVizinhos(clone,linha,coluna,0);
  }
  if (vizinhosVivos < 2 or vizinhosVivos > 3) {
    return MORTA;
  }
  else{
    if (vizinhosVivos == 3){
      return VIVA;
    }
    else{
      if (clone[linha][coluna] == MORTA) {
        return MORTA;
      }
      else {
        return VIVA;
      }
    }
  }
}
////////////////////////////////////////////////////////////////////////////////
int contaVizinhos(char clone[][TAMC],int linha,int coluna,int modo){
  int vizinhosVivos=0,cont,aux;
  switch (modo) {
    case 2:
      if (!linha or linha==TAML-1) {
        if (clone[linha][coluna-1] == VIVA) {
          vizinhosVivos++;
        }
        if (clone[linha][coluna+1] == VIVA) {
          vizinhosVivos++;
        }
        if (!linha) {
          for (cont = coluna-1; cont <= coluna+1; cont++) {
            if (clone[linha+1][cont] == VIVA) {
              vizinhosVivos++;
            }
          }
        } else {
          for (cont = coluna-1; cont <= coluna+1; cont++) {
            if (clone[linha-1][cont] == VIVA) {
              vizinhosVivos++;
            }
          }
        }
      }
      else {
        if (clone[linha-1][coluna] == VIVA) {
          vizinhosVivos++;
        }
        if (clone[linha+1][coluna] == VIVA) {
          vizinhosVivos++;
        }
        if (!coluna) {
          for (cont = linha-1; cont <= linha+1; cont++) {
            if (clone[cont][coluna+1] == VIVA) {
              vizinhosVivos++;
            }
          }
        } else {
          for (cont = linha-1; cont <= linha+1; cont++) {
            if (clone[cont][coluna-1] == VIVA) {
              vizinhosVivos++;
            }
          }
        }
      }
      return vizinhosVivos;
      break;
    case 1:
      if (!linha){
        if (clone[linha+1][coluna] == VIVA) {
          vizinhosVivos++;
        }
        if (!coluna) {
          if (clone[linha][coluna+1] == VIVA) {
            vizinhosVivos++;
          }
          if (clone[linha+1][coluna+1] == VIVA) {
            vizinhosVivos++;
          }
        } else {
          if (clone[linha][coluna-1] == VIVA) {
            vizinhosVivos++;
          }
          if (clone[linha+1][coluna-1] == VIVA) {
            vizinhosVivos++;
          }
        }
      }
      else {
        if (clone[linha-1][coluna] == VIVA) {
          vizinhosVivos++;
        }
        if (!coluna) {
          if (clone[linha][coluna+1] == VIVA) {
            vizinhosVivos++;
          }
          if (clone[linha-1][coluna+1] == VIVA) {
            vizinhosVivos++;
          }
        }
        else {
          if (clone[linha][coluna-1] == VIVA) {
            vizinhosVivos++;
          }
          if (clone[linha-1][coluna-1] == VIVA) {
            vizinhosVivos++;
          }
        }
      }
      return vizinhosVivos;
      break;
    default:
      for (cont = linha-1; cont <= linha+1; cont++) {
        for (aux = coluna-1; aux <= coluna+1; aux++) {
          if(cont == linha and aux == coluna){
            continue;
          }
          else{
            if (clone[cont][aux] == VIVA) {
              vizinhosVivos++;
            }
          }
        }
      }
      return vizinhosVivos;
  }
}
